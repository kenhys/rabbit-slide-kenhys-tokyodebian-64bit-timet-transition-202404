# Tokyo Debian 64bit time_t transition

## How to show

  % rabbit

## How to install

  % gem install rabbit-slide-kenhys-tokyodebian-timet-transition-202404

## How to create PDF

  % rake pdf


# References

* https://wiki.debian.org/ReleaseGoals/64bit-time
  * 64bit time_tのwikiページ
* time_t transition and bugs
  * https://lists.debian.org/debian-devel/2024/03/msg00026.html
  * time_tの進展状況に疑問を呈する投稿
* Re: 64-bit time_t transition in progress in unstable
  * https://lists.debian.org/debian-devel/2024/03/msg00090.html
  * dist-upgradeできないことに対する反応 libuuid1t64がrevertされたことによる影響などの話題
* Re: Re: time_t progress report
  * https://lists.debian.org/debian-devel/2024/03/msg00289.html
  * マイルストーンの進展状況に関する疑念に関する投稿 ブロッカーの進展が数週間ないことについて
    * https://lists.debian.org/debian-devel/2024/03/msg00291.html
      * 状況を改善するためにできることについての意見
* https://lists.debian.org/debian-devel/2024/04/msg00331.html
  * 4/18時点の最新の進捗状況
