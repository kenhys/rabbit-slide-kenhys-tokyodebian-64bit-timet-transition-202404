# Debianの64bit time_t移行は\\n今どうなっているのか

subtitle
:  64bit time_tの進展状況をながめてみよう

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2024年04月 東京エリア・関西合同Debian勉強会

allotted-time
:  25m

theme
:  .

# スライドは\\nRabbit Slide Showにて公開済み

* Debianの64bit time_t移行は今どうなっているのか
  * {::note}<https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-timet-transition-202404>{:/note}

# 本日の内容

* Debianの64bit time_t transitionを追いかけてみよう
  * 64bit time_t移行はなぜ必要なのか
  * 移行計画はどのようになっているのか
  * 移行の進展状況は

## note

今日の発表の狙いは64bit time_tへの移行の状況を知ってもらって、自身が利用しているパッケージの
対応がすすんでいないとか、パッケージがビルドできない問題を抱えているとかいうのを見つけたら
そういった対応作業の手助けをしてもらえるとよい

# 本日の内容

* Debianの64bit time_t transitionを追いかけてみよう
  * **64bit time_t移行はなぜ必要なのか**
  * 移行計画はどのようになっているのか
  * 移行の進展状況は

# 64bit time_t移行はなぜ必要なのか

* Linuxディストリビューションの動向
  * 32bitアーキテクチャのサポートは廃止ないしは縮小の流れ
    * Debianではarmel,armhf,i386が残っている
  * 32bit time_tのままでは2038年問題不可避
    * signed 32bit で扱える範囲 (〜2147483647)

# 2038年問題

* Xデーは2038年1月19日
  * time_tが32bitで時刻を保持しているため32bitアーキテクチャでオーバーフローが発生する問題

```
Time.at(2147483647)
=> 2038-01-19 12:14:07 +0900 
```

## note

2038年問題というが具体的にいつなのかというと1月19日の昼過ぎ(日本時間)
2**31-1=2,147,483,647

# かつての2000年問題との比較

* 2000年問題
  * 西暦下2桁処理で節約したリソースが問題を引き起こす
  * 上記前提で実装されたアプリケーションによる障害発生
    * 事前の対策が進められたことから大混乱は回避された
* 2038年問題
  * システム全体で障害発生{::note}(32bitアーキテクチャにおいて){:/note}


# Debianにおける\\n2038年問題対策の構成要素

  * カーネル
  * glibc
  * ファイルシステム
  * アプリケーション/ライブラリー etc...

# 2038年問題を踏んだらどうなるか(1)


* シェルでの操作不能に陥る

  * 試すならqemuイメージなどがおすすめ

```
$ sudo date --set="2038-01-19 12:14:00"
Tue Jan 19 12:14:00 UTC 2038
$ sudo ls
sudo: unable to open /etc/sudo.conf: Value too large for defined data type
sudo: unable to determine tty: Value too large for defined data type
sudo: unable to get time of day: Value too large for defined data type
sudo: error initializing audit plugin sudoers_audit
```


# Debianにおける\\n2038年問題対策の構成要素

  * カーネル (kernel 5.6以降対応済み)
  * glibc (2.34以降対応済み)
  * ファイルシステム (ext4など対応済み)
  * **アプリケーション/ライブラリー** etc...

## note

https://www.lineo.co.jp/blog/yocto/vol03-2038.html
ext2は対応していないし、kernel 6.9でdeprecated扱いになった
/bootにext2使っている場合は注意が必要かも

https://www.phoronix.com/news/Linux-6.9-Deprecates-EXT2

# アプリケーション・ライブラリーの点検が必要

* 内部で時刻を扱うケースはないか？
* 時刻をどのように保存するようにしているか？

# 事例: Groongaの場合 (1)

* <https://github.com/groonga/groonga/discussions/1698>
* データベースで時刻を扱う型がある
  * Timeカラムは int64_tでマイクロ秒を保持 => 問題なし
  
```
Time.at(9223372036854775807 / 10**6)
=> 294247-01-10 13:00:54 +0900
```

# 事例: Groongaの場合 (2)

* データベースの内部表現(オブジェクトヘッダ)で時刻(更新)を扱う
  * データベース内のオブジェクトを区別するためのシステム情報
  * uint32_tなのでもうすこし頑張れる(2106年問題)

```
Time.at(4294967295)
=> 2106-02-07 15:28:15 +0900 
```


# 2038年問題を踏んだらどうなるか(2)

* 実験: Xデー以降にデータベースを更新する
  * → データベース更新中にクラッシュする💣

```
root@debian:/home/debian/bug1062131# date --set="2038-01-19 12:14:00"
Tue Jan 19 12:14:00 UTC 2038
root@debian:/home/debian/bug1062131# groonga db/test < update-last-modified.grn 
Segmentation fault
```

# 本日の内容

* Debianの64bit time_t transitionを追いかけてみよう
  * 64bit time_t移行はなぜ必要なのか
  * **移行計画はどのようになっているのか**
  * 移行の進展状況は

# Debianにおける64bit time_t transition (1)

* <https://wiki.debian.org/ReleaseGoals/64bit-time>

  * ✅ transitionが必要なABIの変更対象をリストアップする
     * <https://lists.debian.org/debian-devel/2023/05/msg00168.html>
     * 当初少なくとも4900ソースパッケージが影響を受けると考えられていた
  * ✅ dpkg-buildflagが-D_FILE_OFFSET_BITS=64と-D_TIME_BITS=64 を既定で有効にする(i386系を除く)
    * dpkg 1.22.5以降で有効

# Debianにおける64bit time_t transition (2)

* ✅ NMUでライブラリー名変更を実施
  * 進行中は依存関係を満たせずインストールできない、意図せず削除される等が発生
* ✅依存ライブラリーを大量にリビルドしなおす(進行中)


# NMUでライブラリー名変更を実施


* d/controlの例

```diff
-Package: libfoo0
+Package: libfoo0t64
+Provides: ${t64:Provides}
+Replaces: libfoo0
+Breaks: libfoo0 (<< ${source:Version})
```

# t64:Providesの役割

* debhelper 13.14で実装されているマクロ
  * dh_makeshlibsを参照するとよい
  * 32bitアーキテクチャでない or abi=time64が定義されていない
    * 64bit time_t transition関係ないアーキテクチャではt64サフィックスなしのProvides:を定義
    * t64:Providesに `libfoo0 = ${binary:Version}`を設定

## note

libfoo0に依存しているパッケージがあって、libfoo0からlibfoo0t64へ円滑に
アップグレードできるようにするためのもの。

# i386で64bit time_tを\\n有効にするには

* <https://lists.debian.org/debian-devel/2024/02/msg00100.html>
  * 従来i386では既定で64bit time_tは有効にしない
  * パッケージのメンテナーが明示的に指定したら有効になる
    * `DEB_BUILD_OPTIONS=abi=+time64`
  
# リリースチームやメンテナーの動き

* 64bit time_t移行のためのパッチをメンテナーに提供
  * 各パッケージメンテナーは必要に応じてexperimentalでテスト
* NMUでライブラリー名変更まで実施済み
  * メンテナーとしてはFTBFSとかに遭遇しなければとくになし

# 例: 依存関係の\\nトラブルシューティング

* 例えばbuild-depに失敗したときにどうやって調べるか

```
$ apt build-dep groonga
dd-schroot-cmd -c $sid apt-get
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
...

The following packages have unmet dependencies:
 libnorm1t64 : Breaks: libnorm1 (< 1.5.9+dfsg-3.1) but 1.5.9+dfsg-3 is to be installed
 libpgm-5.3-0t64 : Breaks: libpgm-5.3-0 (< 5.3.128~dfsg-2.1) but 5.3.128~dfsg-2 is to be installed
E: Unable to correct problems, you have held broken packages.
Command apt-get --dry-run build-dep -- groonga exited with exit code 1.
```

# 依存関係を調べるのに便利なもの

* apt depends
  * 直接の依存関係を知るのに便利
  * 再帰的な依存関係は一度ではわからない
* apt-rdepends
  * 再帰的な依存関係もわかる
* debtree
  * 再帰的な依存関係を可視化できる

# debtreeによる依存関係の可視化

* 例: groongaの依存関係を可視化

```
debtree --no-recommends groonga > groonga.dot
dot -Tsvg groonga.dot > groonga.svg
```

# debtreeで可視化(一部省略)

![](images/groonga.no-recommends.svg){:relative-height="100"}

# debtreeで可視化するときのヒント

* `--no-recommends`を指定して余計な依存関係を省く
* `--skiplist=`を指定して特定の依存先を省く
  * libc6やドキュメント関係を指定するとわかりやすくなる

```
python3-sphinx
groonga-doc
libc6
gcc-14-base
...
```

# 本日の内容

* Debianの64bit time_t transitionを追いかけてみよう
  * 64bit time_t移行はなぜ必要なのか
  * 移行計画はどのようになっているのか
  * **移行の進展状況は**

# 64bit time_t transitionの進捗状況

* <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-arm@lists.debian.org;tag=time-t>
  * time-t でタグ付けされているバグを参照するとよい
  * すでに1100近くNMUされている
* debian-develメーリングリストを購読していると定期的に進捗報告あり

## note


# 64bit time_t transitionの進捗状況

* <https://qa.debian.org/dose/debcheck/unstable_main/index.html>
  * Packages not installable in scenario unstable_main
  * transition中は頻繁に衝突する
    * => リビルド状況の変化は64bit time_t移行の進展をみるのに使えそう

# およそ一ヶ月ほど前 2024/03/15 

![](images/debcheck20240315.jpeg){:relative-height="100"}

# 先週の状況 2024/04/10

![](images/debcheck20240410.min.png){:relative-height="100"}

# 今週の状況 2024/04/15

![](images/debcheck20240415.min.png){:relative-height="100"}

# dpkgはまだ移行できていない

![](images/dpkg-testing-migrations.png){:relative-height="70"}

* dpkg 1.22.5 はまだtestingに入っていない
  * dpkg >= 1.22.5の指定があるものもtestingに入らない

# 移行進捗の停滞ぎみ(1)

* <https://lists.debian.org/debian-devel/2024/04/msg00331.html>
  * そもそもインストールできない状態のまま
  * FTBFSだけどバグが立っていないので見落とされている
  * 移行前のライブラリ名がハードコーディングされている
  * 移行前と移行後のライブラリー双方への依存で衝突

# 移行進捗の停滞ぎみ(2)

  * 完全に依存先の問題が解消される前にリビルドされた(ので古い)
  * symbol/shlibsが正しくない依存関係になっている
  * まだbinNMUされていない
  
# 64bit time_tでの懸念事項

* <https://lists.debian.org/debian-devel/2024/03/msg00294.html>
  * transitionを進めるため、機能を無効化することがある。再度有効化しないと機能差が発生するかも
* FTBFSが原因でパッケージが削除される方向にすすむことがある
* FTBFSが原因でパッケージが32bitアーキテクチャのサポートをやめてしまうことがある
  * 使っているパッケージが該当していないかは注視が必要


